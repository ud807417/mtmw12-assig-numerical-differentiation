# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 18:36:24 2019

@author: Jake
"""

import numpy as np

# Functions for calculating gradients

def gradient_2point(f, dx):
    '''The gradient of array f assuming points are a distance dx apart
    using 2-point differences'''
    
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    # Two point differences at the end points
    dfdx[0] = (f[1]-f[0]) / dx
    dfdx[-1] = (f[-1]-f[-2]) / dx   # f[-1] because this notes the end of array
    
    # Centred differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1]) / (2*dx)
    return dfdx



def gradient_4point(f, dx):
    '''The gradient of array f assuming points are a distance dx apart
    using 4-point differences'''
    
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    # Points on either end of graph cant use 4 point method as there arent
    # 2 points either side of them. Need to use two point differences for end
    # points.
    dfdx[0] = (f[1]-f[0]) / dx
    dfdx[-1] = (f[-1]-f[-2]) / dx
    
    # Points one after start and one before last also dont have two points
    # either side so cannot use 4 point differences. Here these points use
    # 2 point differences.
    dfdx[1] = (f[2]-f[0]) / (2*dx)
    dfdx[-2] = (f[-1]-f[-3]) / (2*dx)
    
    for i in range(2,len(f)-2):
        dfdx[i] = (-f[i+2]+8*f[i+1]-8*f[i-1]+f[i-2]) / (12*dx)
    return dfdx